import numpy as np


def calculate(list):
    if(len(list) < 9):
        raise ValueError("List must contain nine numbers.")
    a = np.array([[list[0], list[1], list[2]], [list[3], list[4], list[5]],
                  [list[6], list[7], list[8]]])

    calculations = {
        "mean": [np.mean(a, axis=0).tolist(),
                 np.mean(a, axis=1).tolist(),
                 np.mean(a)],
        'variance': [np.var(a, axis=0).tolist(),
                     np.var(a, axis=1).tolist(),
                     np.var(a)],
        'standard deviation':
            [np.std(a, axis=0).tolist(), np.std(a, axis=1).tolist(),
             np.std(a)],
        'max': [np.amax(a, axis=0).tolist(),
                np.amax(a, axis=1).tolist(),
                np.amax(a)],
        'min': [np.amin(a, axis=0).tolist(),
                np.amin(a, axis=1).tolist(),
                np.amin(a)],
        'sum': [np.sum(a, axis=0).tolist(),
                np.sum(a, axis=1).tolist(),
                np.sum(a)],
    }

    return calculations
